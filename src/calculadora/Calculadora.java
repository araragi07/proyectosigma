/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora;

/**
 *
 * @author Estudiantes
 */
public class Calculadora {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // crear una clase operacionaritmetica
        // una clase define un nuevo tipo de dato
        //declarar el objeto
        Operacionaritmetica mioperacion;
        Operaciontrigonometrica miOperacionTrigo;
        Operacionpotenciacion miOperacionPotencia;
        Operacionlogaritmos miOperacionlogaritmos;
        //invocar el metodo constructor
        //inicializar la variable
        mioperacion=new Operacionaritmetica();
        miOperacionTrigo=new Operaciontrigonometrica();
        miOperacionPotencia=new Operacionpotenciacion();
        miOperacionlogaritmos=new Operacionlogaritmos();
        mioperacion.numero1=50;
        mioperacion.numero2=100;
        
        int resultado=mioperacion.suma();
        int resultado1=mioperacion.restar();
        int resultado2=mioperacion.multiplicar();
        int resultado3=mioperacion.dividir();
        System.out.println(mioperacion.suma());
        System.out.println(mioperacion.restar());
        System.out.println(mioperacion.multiplicar());
        System.out.println(mioperacion.dividir());
        
        miOperacionTrigo.numero=45;
        miOperacionTrigo.numero1=0.5;
        System.out.println(miOperacionTrigo.coseno());
        System.out.println(miOperacionTrigo.seno());
        System.out.println(miOperacionTrigo.tangente());
        System.out.println(miOperacionTrigo.arcos());
        System.out.println(miOperacionTrigo.arcsen());
        System.out.println(miOperacionTrigo.arctan());
        
        miOperacionPotencia.numero=2;
        miOperacionPotencia.numero1=3;
        System.out.println(miOperacionPotencia.exponencial());
        System.out.println(miOperacionPotencia.potencia());
        System.out.println(miOperacionPotencia.raiz());
        
        miOperacionlogaritmos.numero=1.5;
        System.out.println(miOperacionlogaritmos.logaritmonepe());
        System.out.println(miOperacionlogaritmos.logaritmo10());
    }
    
}
